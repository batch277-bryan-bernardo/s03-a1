-- [SECTION] INSERTING Records/Row

-- Syntax: INSERT INTO <table_name>(columns) VALUES (values);


INSERT INTO artists(name) VALUES ("Rivermaya");
INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES("Taylor Swift");
INSERT INTO artists(name) VALUES ("New Jeans");
INSERT INTO artists(name) VALUES ("Bamboo");


--  Insert Albums

INSERT INTO albums(album_title, date_released, artist_id)
    VALUES("The album","2020-10-02",2);

INSERT INTO albums (album_title, date_released, artist_id)
    VALUES("Light Peace Love","2005-01-01",5);

INSERT INTO albums (album_title, date_released, artist_id)
    VALUES("As The Music Plays","2004-01-01",5);

INSERT INTO albums(album_title, date_released, artist_id)
    VALUES("Fearless","2008-11-11",3);

INSERT INTO albums(album_title, date_released, artist_id)
    VALUES ("OMG","2020-08-01", 4);

-- Insert Songs 

INSERT INTO songs (song_name, length,genre,album_id)
    VALUES ("Bet You Wanna",239,"KPOP",1);

INSERT INTO songs(song_name, length,genre,album_id)
    VALUES ("Masaya", 450, "Jazz", 3), ("Mr. Clay", 357, "Jazz", 3), ("Noypi", 700, "OPM", 3);


-- Mini Activity Add 2 songs per album

INSERT INTO songs(song_name, length,genre,album_id)
    VALUES ("Breath",424,"Pop",4),("Love Story",355,"Pop",4);

INSERT INTO songs(song_name, length,genre,album_id)    
    VALUES ("OMG",332,"KPOP",5),("Ditto",305,"Electronic Pop Lo-Fi", 5);

INSERT INTO songs (song_name, length,genre,album_id)
    VALUES ("Ice Cream",256,"KPOP",1), ("Lovesick Girls",313,"KPOP",1);

INSERT INTO songs(song_name, length,genre,album_id)
    VALUES ("War of Hearts and Minds", 305, "Jazz", 3), ("Light Years", 228, "Jazz", 3);

INSERT INTO songs(song_name, length,genre,album_id)
    VALUES ("Truth",420,"Alternative Rock",2),("Hallelujah",450,"Alternative Rock",2);


-- [Section] READ data from our database
-- Syntax:
    -- SELECT <column_name> FROM <table_name>;

SELECT * FROM songs;

-- Specific columns that will be shown

SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;

-- To filter the output of the SELECT operation
SELECT * FROM songs WHERE genre = "Electronic Pop Lo-Fi";

SELECT song_name, length FROM songs WHERE genre = "Electronic Pop Lo-Fi";

SELECT * FROM albums WHERE artist_id = 3;

-- We can use AND and OR keyword for multiple expressions in the WHERE clause

-- Display the title and length of the OPM (or other genre) songs that are more than 4 minutes

SELECT song_name, length FROM songs WHERE length > 300 AND genre = "KPOP";

SELECT song_name, length, genre FROM songs WHERE genre = "KPOP" OR genre = "Jazz";

-- [Section] Updating Records/Data
-- UPDATE <table_name> SET <column_name> = <value_to_be> WHERE <condition>

UPDATE songs SET length = 300 WHERE song_name = "Ice Cream";

UPDATE songs SET genre = "K-POP" WHERE genre = "KPOP";

-- From Null to Pop (other's SQL database)

UPDATE songs SET genre = "Pop" WHERE genre is NULL;

-- [Section] Deleting Records
-- DELETE FROM <table_name> WHERE <condition>

-- Delete all Jazz songs that are more than 6 minutes

DELETE FROM songs WHERE genre="OPM" AND length > 600;

-- Mini activity
    -- Delete Jazz songs that are greater than 5 minutes.

DELETE FROM songs WHERE genre ="Jazz" AND length > 400;